package gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PlayContainer extends Container 
{	
	private JPanel background;
	
	private JPanel menuJPanel;
	private JLabel scoreJLabel;
	private JButton startJButton;
	private GameJLabel gameJLabel;
	
	public static final int moveDelay=500;
	public GameJLabel getGameJLabel() {
		return gameJLabel;
	}
	public PlayContainer()
	{
		super();	
		background= new JPanel();
		background.setLayout(new GridLayout(1,1));
		background.setBackground(Color.BLACK);
		GridLayout layout = new GridLayout(1,2);
		this.setLayout(layout);
		gameJLabel = new GameJLabel();
		
		menuJPanel = new JPanel();
		menuJPanel.setBackground(Color.BLACK);
		menuJPanel.setLayout(new BoxLayout(menuJPanel, BoxLayout.Y_AXIS));

		scoreJLabel = new JLabel("score: 0");
		startJButton = new JButton("start");
		
		scoreJLabel.setForeground(Color.WHITE);
		startJButton.setBackground(Color.BLACK);
		startJButton.setForeground(Color.WHITE);
		startJButton.setFocusable(false);
		
		
		menuJPanel.add(Box.createRigidArea(scoreJLabel.getPreferredSize()));
		menuJPanel.add(Box.createRigidArea(scoreJLabel.getPreferredSize()));
		menuJPanel.add(scoreJLabel);
		menuJPanel.add(Box.createRigidArea(scoreJLabel.getPreferredSize()));
		menuJPanel.add(Box.createRigidArea(scoreJLabel.getPreferredSize()));
		menuJPanel.add(Box.createRigidArea(scoreJLabel.getPreferredSize()));
		menuJPanel.add(Box.createRigidArea(scoreJLabel.getPreferredSize()));
		menuJPanel.add(startJButton);
		
		background.add(gameJLabel);
		background.add(menuJPanel);
		this.add(background);
	}
	public void setStartJButtonClickEvent(ActionListener listener) {
		startJButton.addActionListener(listener);
	}
	public void showMovesAnimation(ArrayList<Integer> realMoves) {
		Timer timer = new Timer();
		long delay=moveDelay;
		for(int i=0;i<realMoves.size();i++)
		{
			timer.schedule(new TimerTaskMove(realMoves.get(i)),delay);
			delay+=moveDelay;
		}
		timer.schedule(new TimerTaskMove(-1),delay);
		
		
	}
	private void showMove(int move) 
	{
		switch (move)
		{
		case -1:
			gameJLabel.setIcon(ResourcesLoader.getInstance().getNormalIcon());
			break;
		case 1:
			gameJLabel.setIcon(ResourcesLoader.getInstance().getRedIcon());
			ResourcesLoader.getInstance().play(ResourcesLoader.getInstance().getRedAIS());
			break;
		case 2:
			gameJLabel.setIcon(ResourcesLoader.getInstance().getGreenIcon());
			ResourcesLoader.getInstance().play(ResourcesLoader.getInstance().getGreenAIS());
			break;
		case 3:
			gameJLabel.setIcon(ResourcesLoader.getInstance().getYellowIcon());
			ResourcesLoader.getInstance().play(ResourcesLoader.getInstance().getYellowAIS());
			break;
		case 4:
			gameJLabel.setIcon(ResourcesLoader.getInstance().getBlueIcon());
			ResourcesLoader.getInstance().play(ResourcesLoader.getInstance().getBlueAIS());
			break;
		}
	}
	public void setScore(int score) {
		scoreJLabel.setText("score: " +score);
			
	}
	
	private class TimerTaskMove extends TimerTask
	{
		int move;
		public TimerTaskMove(int move)
		{
			super();
			this.move=move;
		}
		@Override
		public void run() {
			showMove(move);
		}
		
	}


}
