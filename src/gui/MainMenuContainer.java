package gui;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

public class MainMenuContainer extends Container {
	private JButton playJButton;
	private JButton hallOfFameJButton;
	private JButton aboutJButton;

	public MainMenuContainer() 
	{
		super();
		playJButton=new MenuJButton("play");
		hallOfFameJButton = new MenuJButton("hall of fame");
		aboutJButton = new MenuJButton("about");
		
		
		GridLayout layout = new GridLayout(1,5);
		this.setLayout(layout);

		JPanel northPanel = new JPanel();
		JPanel westPanel = new JPanel();
		JPanel centerPanel = new JPanel();
		JPanel eastPanel = new JPanel();
		JPanel southPanel = new JPanel();

		northPanel.setBackground(Color.BLACK);
		westPanel.setBackground(Color.BLACK);
		centerPanel.setBackground(Color.BLACK);
		eastPanel.setBackground(Color.BLACK);
		southPanel.setBackground(Color.BLACK);

		//BoxLayout boxlayout = new BoxLayout(centerPanel, BoxLayout.Y_AXIS);
		GridLayout boxlayout = new GridLayout(10,1,40,20);
	
		centerPanel.setLayout(boxlayout); 
		
		centerPanel.add(Box.createHorizontalGlue());
		centerPanel.add(playJButton);
		centerPanel.add(Box.createHorizontalGlue());
		centerPanel.add(hallOfFameJButton);
		centerPanel.add(Box.createHorizontalGlue());
		centerPanel.add(Box.createHorizontalGlue());
		centerPanel.add(aboutJButton);
		
		this.add(northPanel);
		this.add(westPanel);
		this.add(centerPanel);
		this.add(eastPanel);
		this.add(southPanel);
	}
	public void setPlayJButtonClickEvent(ActionListener listener)
	{
		playJButton.addActionListener(listener);
	}
	public void setHallOfFameJButtonClickEvent(ActionListener listener)
	{
		hallOfFameJButton.addActionListener(listener);
	}
	public void setAboutJButtonClickEvent(ActionListener listener)
	{
		aboutJButton.addActionListener(listener);
	}
}
