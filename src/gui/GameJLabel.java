package gui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JLabel;

public class GameJLabel extends JLabel
{
	public GameJLabel()
	{
		super(ResourcesLoader.getInstance().getNormalIcon());
	}
	private boolean isMouseInCircle(MouseEvent arg0)
	{
		int r = 100;
		int x1=arg0.getX();
		int y1=arg0.getY();
		double x2=this.getSize().getWidth()/2;
		double y2=this.getSize().getHeight()/2;
		double d = Math.sqrt(Math.pow(x2-x1, 2)+Math.pow(y2-y1, 2));
		return (d<r);
	}
	public int calculateMouseQuarter(MouseEvent arg0) 
	{
		if (GameJLabel.this.isMouseInCircle(arg0)) 
		{
			int x1 = arg0.getX();
			int y1 = arg0.getY();
			double x2 = GameJLabel.this.getSize().getWidth() / 2;
			double y2 = GameJLabel.this.getSize().getHeight() / 2;
			if (x1 > x2 && y1 > y2) {
				return 4;
			} else if (x1 < x2 && y1 < y2) {
				return 2;
			} else if (x1 < x2 && y1 > y2) {
				return 3;
			} else if (x1 > x2 && y1 < y2) {
				return 1;
			}
		}
		return -1;
	}
	public void changeIcon(MouseEvent arg0)
	{
		switch (calculateMouseQuarter(arg0))
		{
		case -1:
			setIcon(ResourcesLoader.getInstance().getNormalIcon());
			break;
		case 1:
			setIcon(ResourcesLoader.getInstance().getRedIcon());
			break;
		case 2:
			setIcon(ResourcesLoader.getInstance().getGreenIcon());
			break;
		case 3:
			setIcon(ResourcesLoader.getInstance().getYellowIcon());
			break;
		case 4:
			setIcon(ResourcesLoader.getInstance().getBlueIcon());
			break;
		}
	}
}
