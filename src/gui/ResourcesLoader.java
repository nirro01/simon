package gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class ResourcesLoader 
{
	private static ResourcesLoader instance=null;
	
	
	private ImageIcon normalIcon;
	private ImageIcon greenIcon;
	private ImageIcon redIcon;
	private ImageIcon blueIcon;
	private ImageIcon yellowIcon;
	
	private AudioInputStream greenAIS;
	private AudioInputStream  redAIS;
	private AudioInputStream  blueAIS;
	private AudioInputStream  yellowAIS;
	private AudioInputStream  noAIS;
	
	private Clip clip;
	private ResourcesLoader()
	{
		try {
			//images
			File file1 = new File("./resources/graphics/normal.png"); 
			File file2 = new File("./resources/graphics/green.png"); 
			File file3 = new File("./resources/graphics/red.png"); 
			File file4 = new File("./resources/graphics/blue.png"); 
			File file5 = new File("./resources/graphics/yellow.png"); 
			BufferedImage normalImage = ImageIO.read(file1);
			BufferedImage greenImage = ImageIO.read(file2);
			BufferedImage redImage = ImageIO.read(file3);
			BufferedImage blueImage = ImageIO.read(file4);
			BufferedImage yellowImage = ImageIO.read(file5);
			normalIcon= new ImageIcon(normalImage);
			greenIcon = new ImageIcon(greenImage);
			redIcon = new ImageIcon(redImage);
			blueIcon = new ImageIcon(blueImage);
			yellowIcon = new ImageIcon(yellowImage);
			
			//sounds
			File file6 = new File("./resources/sounds/1.wav"); 
			File file7 = new File("./resources/sounds/2.wav"); 
			File file8 = new File("./resources/sounds/3.wav"); 
			File file9 = new File("./resources/sounds/4.wav"); 
			File file10 = new File("./resources/sounds/no.wav"); 
			greenAIS = AudioSystem.getAudioInputStream(file6);
			redAIS = AudioSystem.getAudioInputStream(file7);
			blueAIS = AudioSystem.getAudioInputStream(file8);
			yellowAIS = AudioSystem.getAudioInputStream(file9);
			noAIS = AudioSystem.getAudioInputStream(file10);
			
		} catch (IOException | UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
	}
	public void play(AudioInputStream sound)
	{
		try {
			clip=AudioSystem.getClip();
			clip.close();
			clip=AudioSystem.getClip();
			clip.open(sound);
			clip.start();
		} catch (LineUnavailableException | IOException e) {
			e.printStackTrace();
		}
	}
	public static ResourcesLoader getInstance()
	{
		if(instance==null)
			return new ResourcesLoader();
		return instance;
	}
	public ImageIcon getNormalIcon() {
		return normalIcon;
	}
	public ImageIcon getGreenIcon() {
		return greenIcon;
	}
	public ImageIcon getRedIcon() {
		return redIcon;
	}
	public ImageIcon getBlueIcon() {
		return blueIcon;
	}
	public ImageIcon getYellowIcon() {
		return yellowIcon;
	}
	public AudioInputStream getGreenAIS() {
		return greenAIS;
	}
	public AudioInputStream getRedAIS() {
		return redAIS;
	}
	public AudioInputStream getBlueAIS() {
		return blueAIS;
	}
	public AudioInputStream getYellowAIS() {
		return yellowAIS;
	}
	public AudioInputStream getNoAIS() {
		return noAIS;
	}

}
