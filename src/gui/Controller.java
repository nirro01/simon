package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Timer;
import java.util.TimerTask;

import logic.Game;
import logic.State;



public class Controller 
{
	private Game game;
	private MainMenuContainer mainMenuContainer;
	private PlayContainer playContainer;
	private HallOfFameContainer hallOfFameContainer;
	private AboutContainer aboutContainer;
	private MainJFrame mainJFrame;
	private boolean busy;
	
	public Controller()
	{
		construct();
		setInitialState();
		setChangeContentEventsConfigurations();	
		setGameMouseListeners();
		setStartGameClickEvent();
	}
	private void setGameMouseListeners()
	{
		MouseEventsHandler mouseEventsHandler = new MouseEventsHandler();
		playContainer.getGameJLabel().addMouseListener(mouseEventsHandler);
		playContainer.getGameJLabel().addMouseMotionListener(mouseEventsHandler);
	}
	private void setStartGameClickEvent() 
	{
		playContainer.setStartJButtonClickEvent(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				game.initilize();
				playContainer.showMovesAnimation(game.getRealMoves());
				game.setState(State.waitForInput);
			}
		});
	}
	private void construct() {
		mainJFrame = new MainJFrame();
		mainMenuContainer = new MainMenuContainer();
		playContainer = new PlayContainer();
		hallOfFameContainer = new HallOfFameContainer();
		aboutContainer = new AboutContainer();	
		game = new Game(); 
	}
	private void setInitialState() 
	{
		busy=false;
		mainJFrame.setContentPane(mainMenuContainer);
		mainJFrame.setVisible(true);
	}
	private void setChangeContentEventsConfigurations() 
	{
		SwitchWindowActionListenerBuilder builder = new SwitchWindowActionListenerBuilder(mainJFrame);
		mainMenuContainer.setPlayJButtonClickEvent(builder.build(playContainer));	
		mainMenuContainer.setHallOfFameJButtonClickEvent(builder.build(hallOfFameContainer));	
		mainMenuContainer.setAboutJButtonClickEvent(builder.build(aboutContainer));
	}
	private class MouseEventsHandler implements MouseListener, MouseMotionListener 
	{

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			if(!busy)
			{
				playContainer.getGameJLabel().changeIcon(e);
			}
			
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (!busy) 
			{
				if (game.getState() == logic.State.waitForInput) 
				{
					int quarter = playContainer.getGameJLabel().calculateMouseQuarter(e);
					switch (quarter) {
					case -1:
						return;
					case 1:
						ResourcesLoader.getInstance().play(
								ResourcesLoader.getInstance().getRedAIS());
						break;
					case 2:
						ResourcesLoader.getInstance()
								.play(ResourcesLoader.getInstance()
										.getGreenAIS());
						break;
					case 3:
						ResourcesLoader.getInstance().play(
								ResourcesLoader.getInstance()
										.getYellowAIS());
						break;
					case 4:
						ResourcesLoader.getInstance().play(
								ResourcesLoader.getInstance().getBlueAIS());
						break;
					}
					game.playMove(quarter);
				}
				checkGameState();
			}
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		private void checkGameState()
		{
			if(game.getState()==logic.State.roundCompleted)
			{
				//test commit
				busy=true;
				playContainer.showMovesAnimation(game.getRealMoves());
				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					
					@Override
					public void run() {
						busy=false;
						game.setState(State.waitForInput);
						
					}
				}, game.getRealMoves().size()*PlayContainer.moveDelay);
				playContainer.setScore(game.getScore());
				
			}
			else if (game.getState()==logic.State.gameOver)
			{
				ResourcesLoader.getInstance().play(ResourcesLoader.getInstance().getNoAIS());
				
			}
		}
	}
}
