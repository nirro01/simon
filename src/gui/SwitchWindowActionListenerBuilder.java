package gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class SwitchWindowActionListenerBuilder 
{
	protected JFrame jframe;

	SwitchWindowActionListenerBuilder(JFrame source) 
	{
		this.jframe = source;
	}

	public ActionListener build(final Container container) 
	{
		return new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				jframe.getContentPane().removeAll();
				jframe.getContentPane().add(container);
				jframe.revalidate();
				jframe.repaint();

			}
		};
	}

}
