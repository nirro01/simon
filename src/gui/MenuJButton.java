package gui;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

public class MenuJButton extends JButton
{
	public MenuJButton()
	{
		super();
		construct();
	}
	public MenuJButton(String text)
	{
		super(text);
		construct();
	}
	private void construct()
	{
		this.setFocusable(false);
		this.setBackground(Color.WHITE);
		this.addMouseListener(new MenuJButtonMouseListener());
	}
	public class MenuJButtonMouseListener implements MouseListener
	{

		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseEntered(MouseEvent arg0) {
			MenuJButton.this.setBackground(Color.cyan);
			
		}

		public void mouseExited(MouseEvent arg0) {
			MenuJButton.this.setBackground(Color.WHITE);
			
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
