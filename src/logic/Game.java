package logic;

import java.util.ArrayList;
import java.util.Random;

public class Game 
{
	private int score;
	private State state;
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	private ArrayList<Integer> realMoves;
	public ArrayList<Integer> getRealMoves() {
		return realMoves;
	}
	private ArrayList<Integer> userMoves;
	
	//public static final int red=1;
	//public static final int green=2;
	//public static final int yellow=3;
	//public static final int blue=4;
	public Game()
	{
		realMoves = new ArrayList<Integer>();
		userMoves= new ArrayList<Integer>();
	}
	public void initilize()
	{
		realMoves.clear();
		userMoves.clear();
		score=0;
		generateRandomMove();
	}
	public void generateRandomMove() 
	{
		cleanUserMoves();
		Random random = new Random();
		int move = random.nextInt(4) + 1; //1,2,3,4
		realMoves.add(move);
		state=State.roundCompleted;
	}
	
	private void cleanUserMoves()
	{
		userMoves.clear();
	}
	public State playMove(int move)
	{
		userMoves.add(move);
		updateState();
		return state;
	}
	private void updateState() 
	{
		for(int i=0;i<userMoves.size();i++)
			if(!userMoves.get(i).equals(realMoves.get(i)))
			{
				state=State.gameOver;
				return;
			}
		if(userMoves.size()==realMoves.size())
		{
			generateRandomMove();
			state=State.roundCompleted;
			score++;
			return;
		}
		else
		{
			state=State.waitForInput;
		}
	}
	public int getScore() {
		// TODO Auto-generated method stub
		return score;
	}
}
