package logic;

public enum State 
{
	waitForInput,
	roundCompleted,
	gameOver; 
}
